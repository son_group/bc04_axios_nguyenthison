function Student(studentID, studentName, email, password, math, physics, chemistry){
    this.studentID = studentID;
    this.studentName = studentName;
    this.email = email;
    this.password = password;
    this.math = math*1;
    this.physics = physics*1;
    this.chemistry = chemistry*1;
    this.averagePoint = function(){
        return (this.math  + this.physics+this.chemistry)/3;
    };
}