var validation = {
    checkEmptyInput: function(value, idError, message){
        if(value.length ==0){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    checkLength: function(value, idError, message, minlength, maxlength){
        if(value.length <minlength || value.length>maxlength){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    },
    checkEmailValid: function(value,idError,message){
        const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if(value.toLowerCase().match(re)){
            document.getElementById(idError).innerText = "";
            return true;
        }else{
            document.getElementById(idError).innerText = message;
            return false;
        }
    },
    checkPointValid: function(value, idError,message,minPoint,maxPoint){
        if(value <minPoint || value>maxPoint){
            document.getElementById(idError).innerText = message;
            return false;
        }else{
            document.getElementById(idError).innerText = "";
            return true;
        }
    }
}

var validateString = {
    checkStringIncludeStringFind: function(stringValue, subStringValue){
        if(stringValue.toLowerCase().includes(subStringValue.toLowerCase())){
            return true;
        }else{
            return false;
        }        
    }
}