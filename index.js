const BASE_URL = "https://62db6cabe56f6d82a7728576.mockapi.io/";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
    // data:{}
  })
    .then(function (res) {
      tatLoading();
      // console.log(res);
      var studentList = studentListAveragePoint(res.data);      
      console.log("Student List: ",studentList);
      // console.log("Change Student: ",changeStudent);
      renderDSSV(studentList);
    })
    .catch(function (err) {
      tatLoading();
      // console.log(err);
    });
}
getDSSV();
clearForm();

function themSV() {
  var student = getStudentInfoFromForm();

  var isValid = validation.checkEmptyInput(
    student.studentName,
    "spanTenSV",
    "Tên sinh viên không được để trống"
  );

  isValid =
    isValid &
    (validation.checkEmptyInput(
      student.email,
      "spanEmailSV",
      "Email không được để trống"
    ) &&
      validation.checkEmailValid(
        student.email,
        "spanEmailSV",
        "Email không hợp lệ"
      ));

  isValid =
    isValid &
    validation.checkEmptyInput(
      student.password,
      "spanMatKhau",
      "Mật khẩu không được để trống"
    );

  isValid =
    isValid &
    (validation.checkEmptyInput(
      student.math,
      "spanToan",
      "Điểm Toán không được để trống"
    ) &&
      validation.checkPointValid(
        student.math,
        "spanToan",
        "Điểm Toán không được nhỏ hơn 0 hoặc lớn hơn 10",
        0,
        10
      ));

  isValid =
    isValid &
    (validation.checkEmptyInput(
      student.physical,
      "spanLy",
      "Điểm Lý không được để trống"
    ) &&
      validation.checkPointValid(
        student.physical,
        "spanLy",
        "Điểm Lý không được nhỏ hơn 0 hoặc lớn hơn 10",
        0,
        10
      ));

  isValid =
    isValid &
    (validation.checkEmptyInput(
      student.chemistry,
      "spanHoa",
      "Điểm Hoá không được để trống"
    ) &&
      validation.checkPointValid(
        student.chemistry,
        "spanHoa",
        "Điểm Hoá không được nhỏ hơn 0 hoặc lớn hơn 10",
        0,
        10
      ));

  if (isValid) {
    batLoading();
    axios({
      url: `${BASE_URL}/sv/`,
      method: "POST",
      data: student,
    })
      .then(function (res) {
        console.log("res.data: ",res.data);
        tatLoading();
        getDSSV(res.data);
        clearForm();
      })
      .catch(function (err) {
        tatLoading();
        console.log(err);
      });
  }
}

function editStudent(id) {
  batLoading();
  document.getElementById("updateStudentInfo").disabled = false;
  document.getElementById("addNewStudent").disabled = true;
  
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
    // data:{}
  })
    .then(function (res) {
      tatLoading();
      var student = res.data;

      showStudentInforToForm(student);

      // document.getElementById("updateStudentInfo").disabled = false;
    })
    .catch(function (err) {
      tatLoading();
      // console.log(err);
    });
}

function updateStudent() {
  var student = getStudentInfoFromForm();
  var id = student.studentID;
  var isValid = validation.checkEmptyInput(
    student.studentName,
    "spanTenSV",
    "Tên sinh viên không được để trống"
  );

  isValid =
    isValid &
    (validation.checkEmptyInput(
      student.email,
      "spanEmailSV",
      "Email không được để trống"
    ) &&
      validation.checkEmailValid(
        student.email,
        "spanEmailSV",
        "Email không hợp lệ"
      ));

  isValid =
    isValid &
    validation.checkEmptyInput(
      student.password,
      "spanMatKhau",
      "Mật khẩu không được để trống"
    );

  isValid =
    isValid &
    (validation.checkEmptyInput(
      student.math,
      "spanToan",
      "Điểm Toán không được để trống"
    ) &&
      validation.checkPointValid(
        student.math,
        "spanToan",
        "Điểm Toán không được nhỏ hơn 0 hoặc lớn hơn 10",
        0,
        10
      ));

  isValid =
    isValid &
    (validation.checkEmptyInput(
      student.physics,
      "spanLy",
      "Điểm Lý không được để trống"
    ) &&
      validation.checkPointValid(
        student.physics,
        "spanLy",
        "Điểm Lý không được nhỏ hơn 0 hoặc lớn hơn 10",
        0,
        10
      ));

  isValid =
    isValid &
    (validation.checkEmptyInput(
      student.chemistry,
      "spanHoa",
      "Điểm Hoá không được để trống"
    ) &&
      validation.checkPointValid(
        student.chemistry,
        "spanHoa",
        "Điểm Hoá không được nhỏ hơn 0 hoặc lớn hơn 10",
        0,
        10
      ));

  if (isValid) {
    batLoading();
    axios({
      url: `${BASE_URL}/sv/${id}`,
      method: "PUT",
      data: student,
    })
      .then(function (res) {
        tatLoading();
        getDSSV(res.data);
      })
      .catch(function (err) {
        tatLoading();
        console.log(err);
      });
  }
}

function xoaSinhVien(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
      getDSSV();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function resetForm(){
  clearForm();
}